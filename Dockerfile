FROM python:3.8.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /site
WORKDIR /site
COPY requirements.txt /site/
RUN pip install -r requirements.txt
WORKDIR /site/mysite